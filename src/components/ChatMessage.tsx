import * as React from 'react';
import { Message } from '../model/Message';

export const ChatMessage = (props: { message: Message, currentUsername: string }) => {
  return (
    <div className={"chat-message " + (props.currentUsername === props.message.username ? "chat-message-self" : "chat-message-other")}>
      <b>{props.message.username}</b>:&nbsp;{props.message.text}
    </div>
  );
}