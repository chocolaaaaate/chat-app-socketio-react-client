import * as React from 'react';

export const Header = (props) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <span className="navbar-brand">React+Socket.IO Chat App</span>
            <span>{props.isConnectedToServer ? <span className="badge badge-success">CONNECTED</span> : <span className="badge badge-danger">DISCONNECTED</span>}&nbsp;{props.username ? (<span>Chatting as: <b>{props.username}</b></span>) : ""}</span>
        </nav>
    );
}
