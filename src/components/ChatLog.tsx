import * as React from 'react';
import { Message } from '../model/Message';
import { ChatMessage } from './ChatMessage';

export const ChatLog = (props: { messages: Message[], currentUsername: string, whoIsTyping: string }) => {
    return (
        <div>
            <div className="chat-log">
                {props.messages.map(msg => <ChatMessage key={Math.random()} currentUsername={props.currentUsername} message={msg} />)}
                <i className="chat-feedback">{props.whoIsTyping !== "" ? `${props.whoIsTyping} is typing` : ""}</i>
            </div>
        </div>
    );
}