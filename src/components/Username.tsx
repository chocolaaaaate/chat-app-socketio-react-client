import * as React from 'react';

export const Username = (props) => {
    return (
        <div className="input-group mb-3">
            <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">Name</span>
            </div>
            <input
                onChange={e => props.setUsernameCallback(e.target.value)}
                type="text" className="form-control" placeholder="Your name" />
        </div>
    );
}