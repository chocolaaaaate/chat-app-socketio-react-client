import * as React from 'react';

export class ChatMessageEntry extends React.Component
    <
    {
        sendMessageCallback: (msg: string) => any,
        userIsTypingCallback: (isTyping: boolean) => any
    },
    { text: string }
    > {

    constructor(props) {
        super(props);
        this.state = {
            text: ""
        }
    }

    setText = (text) => {
        this.setState({
            text: text
        });
    }

    render() {
        return (
            <div className="input-group mb-3 chat-entry">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">Message</span>
                </div>
                <input
                    value={this.state.text}
                    onChange={(ev) => {
                        this.setText(ev.target.value);
                        this.props.userIsTypingCallback(true);
                    }}
                    type="text" className="form-control" placeholder="Type your message here" />
                <div className="input-group-append">
                    <button
                        onClick={() => {
                            this.setText("");
                            this.props.userIsTypingCallback(false);
                            this.props.sendMessageCallback(this.state.text)
                        }}
                        className="btn btn-secondary" type="button">SEND</button>
                </div>
            </div>
        );
    }
}
