import React from 'react';
import './App.css';
import * as io from 'socket.io-client';
import { Username } from './components/Username';
import { ChatLog } from './components/ChatLog';
import { Header } from './components/Header';
import { ChatMessageEntry } from './components/ChatMessageEntry';
import { Message } from './model/Message';

class App extends React.Component<{},
  {
    username: string,
    messages: Message[],
    whoIsTyping: string,
    isConnectedToServer: boolean
  }> {

  socket: any;
  username: string;

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      messages: [],
      whoIsTyping: "",
      isConnectedToServer: false
    }
  }

  componentDidMount() {
    this.socket = io.connect('http://localhost:80');

    this.socket.on('connect', () => {
      this.setState({
        isConnectedToServer: true
      });
    });
    /* The socket.on('connect') is an event which is fired upon a successful connection from the web browser. */

    this.socket.on('disconnect', () => {
      console.log("disconnected");
      this.setState({
        isConnectedToServer: false
      });
    });

    this.socket.on('chat', (messageEmittedFromServer) => {
      let updatedMessages = [...this.state.messages, messageEmittedFromServer];
      this.setState({
        messages: updatedMessages
      });
    });

    this.socket.on('typing', (messageEmittedFromServer) => {
      if (messageEmittedFromServer.isTyping) {
        this.setState({
          whoIsTyping: messageEmittedFromServer.who
        });
      } else {
        this.setState({
          whoIsTyping: ""
        });
      }
    });
  }

  setUsername = (uname) => {
    this.setState({
      username: uname
    });
  }

  sendMessage = (msg) => {
    let message: Message = {
      username: this.state.username,
      text: msg
    };

    this.socket.emit('chat', message);
  }

  userIsTyping = (isTyping: boolean) => {
    this.socket.emit('typing',
      {
        who: this.state.username,
        isTyping: isTyping
      }
    ); // says I'm typing
  }

  render() {
    return (
      <div>
        <Header username={this.state.username} isConnectedToServer={this.state.isConnectedToServer} />
        <div style={{padding: "0rem 1rem 1rem 1rem"}}>
          <ChatLog messages={this.state.messages} currentUsername={this.state.username} whoIsTyping={this.state.whoIsTyping} />
          <ChatMessageEntry
            sendMessageCallback={this.sendMessage}
            userIsTypingCallback={this.userIsTyping} />
          <Username setUsernameCallback={this.setUsername} />
        </div>
      </div>
    );
  }
}

export default App;
